from flask import Flask
from flask_restful import Resource, Api


app = Flask(__name__)
api = Api(app)
#print(type(app))
#print(type(api))

## Code Our Flask API 
## we need muffing to be supplied as JSON 
class Product(Resource):
    def get(self):
        return {
            "products": ["Beer Induced muffin", "Double Chocolate", "Duffin!", "Rasin Muffin"]
        }

class Product_price(Resource):
    def get(self):
        return {
            "price": [12, 10, 17, 10] 
        }

# Expose this in the API
api.add_resource(Product, '/')
api.add_resource(Product_price, '/price')

if __name__ == '__main__':
    print("This is the MAIN!")
    print("use this block to make an app have different behaviours when called directly")
    app.run("0.0.0.0", port=80, debug=True)