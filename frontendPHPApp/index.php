<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>This is a garth tab</title>
</head>
<body>
    <h1>This is our amazing store of foods</h1>
    <h2>This is a great store... really go all out</h2>
    <p>Please see our available muffins and duffins</p>

    <ul>
        <?php
            $json = file_get_contents('http://product-api/');
            $objects = json_decode($json);

            $products = $objects->products;

            foreach ($products as $product) {
                echo "<li>$product</li>";
            }
        ?>
    </ul>    
</body>
</html>
