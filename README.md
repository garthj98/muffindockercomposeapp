# Muffin Docker Compose App

This will be an app to showcase docker compose. We will build it with a php html front page and another container with a python API simulating a database.

We will use this class to cover:

- Dev environments 
- Testing and CD environments 
- Git and github branching 
- Git branching pracrices and git etiquete 
- Docker and docker compose 

We will also touch on:

- Python 
- API
- Webapps 
- Seperation of concerns 

EXTRA OBJECTIVES 

- Add CI
- Add CD

### Plan 

1. Create simple API with python. Put it in a container.
2. Create simple PHP app to parse/consume JSON sent from API
3. Build our docker compose.

```bash 
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt 
python3 app.py

docker build -t muffin_api .

docker compose up -d
docker compose down

```